/*
 * elevator_logic.h
 *
 *  Created on: 7 Feb 2013
 *      Author: nkigen
 */

#ifndef ELEVATOR_LOGIC_H_
#define ELEVATOR_LOGIC_H_
#include "elevator_graphics.h"
#include<stdlib.h>

#define PERIOD_MAIN 100
#define PERIOD_CONTROL_LEFT 10
#define PERIOD_CONTROL_RIGHT 30
#define PERIOD_REQUEST 2000
#define REQ_THRESHHOLD 40
int getNearestElevator(int current_floor,int target);
void moveCabin(unsigned int evelator);
void serveRequest(unsigned int elevator);
void createRequest(void);
void requestElevator(unsigned int from, unsigned int to);
void time_add_ms(struct timespec *t, int ms);
void pickPerson(REQUEST rq,unsigned int elevator,REQUEST *r);
int getDirection(unsigned int elevator);
void AlightPerson(unsigned int elevator,PERSON *p);
void checkFloors(int j);

#endif /* ELEVATOR_LOGIC_H_ */
