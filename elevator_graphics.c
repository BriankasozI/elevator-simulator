/*
 * elevator.c
 *
 *  Created on: 6 Feb 2013
 *      Author: nkigen
 */
#include "elevator_graphics.h"

char disp_text[MAX_TEXT_LEN];
extern unsigned int NUM_FLOORS;
void buildingInit() {
	float fh;

	building.w = SCREEN_W * 0.9 - SCREEN_W * 0.05;
	building.h = SCREEN_H * 0.9 - SCREEN_H * 0.12;
	building.pending_requests = 0;
	building.total_requests = 0;
	fh = (building.h / (NUM_FLOORS));

	int i;

	for (i = 0; i < MAX_REQUESTS; i++)
		building.request_queue[i].status = REQUEST_SERVED;
	building.floors = (FLOOR *) malloc(sizeof(FLOOR) * NUM_FLOORS);
	for (i = 0; i < NUM_FLOORS; i++) {
		building.floors[i].id = NUM_FLOORS - i - 1;
		building.floors[i].num_waiting = 0;
		building.floors[i].h = fh;
		building.floors[i].w = building.w * 0.4;
		building.floors[i].x = SCREEN_W * 0.6;
		building.floors[i].y = SCREEN_H * 0.125 + fh * i;
		building.floors[i].h = fh + building.floors[i].y
				- (building.h / (NUM_FLOORS + 1)) / NUM_FLOORS;
		printf("%d %f %f\n", i, building.floors[i].y, building.floors[i].x);
		int j = 0;
		for (j = 0; j < MAX_CAPACITY_FLOOR; j++)
			building.floors[i].people[j] = NULL;
	}

	building.left_elevator.x = SCREEN_W * 0.15;
	building.left_elevator.y = SCREEN_H * 0.125;
	building.left_elevator.w = building.w * 0.39;
	building.left_elevator.h = building.floors[i - 1].h;

	building.right_elevator.x = building.w - building.floors[i - 1].w
			+ SCREEN_W * 0.1;
	building.right_elevator.y = SCREEN_H * 0.125;
	building.right_elevator.w = building.w - building.w * 0.39
			+ building.floors[i - 1].w - 50;
	building.right_elevator.h = building.floors[i - 1].h;

	building.left_elevator.cabin.max_capacity = MAX_CAPACITY;
	building.left_elevator.cabin.current_floor = INIT_FLOOR_LEFT;
	building.left_elevator.cabin.current_capacity = 0;
	building.left_elevator.cabin.status = ELEVATOR_STOP;
	building.left_elevator.cabin.direction = DIRECTION_NONE;
	building.left_elevator.served_requests = 0;

	building.left_elevator.cabin.x = building.left_elevator.x;
	building.left_elevator.cabin.y =
			building.left_elevator.y
					+ (NUM_FLOORS - 1 - INIT_FLOOR_LEFT)
							* ((building.h / (NUM_FLOORS + 1)) / NUM_FLOORS
									+ building.floors[NUM_FLOORS - 1
											- INIT_FLOOR_LEFT].h
									- building.floors[NUM_FLOORS - 1
											- INIT_FLOOR_LEFT].y);
	building.left_elevator.cabin.w = building.left_elevator.w;
	building.left_elevator.cabin.h = building.left_elevator.cabin.y
			+ building.floors[NUM_FLOORS - 1 - INIT_FLOOR_LEFT].h
			- building.floors[NUM_FLOORS - 1 - INIT_FLOOR_LEFT].y;

	building.right_elevator.cabin.max_capacity = MAX_CAPACITY;
	building.right_elevator.cabin.current_floor = INIT_FLOOR_RIGHT;
	building.right_elevator.cabin.current_capacity = 0;
	building.right_elevator.cabin.direction = DIRECTION_NONE;
	building.right_elevator.cabin.status = ELEVATOR_STOP;
	building.right_elevator.served_requests = 0;

	building.right_elevator.cabin.x = building.right_elevator.x;
	building.right_elevator.cabin.y =
			building.right_elevator.y
					+ (NUM_FLOORS - 1 - INIT_FLOOR_RIGHT)
							* ((building.h / (NUM_FLOORS + 1)) / NUM_FLOORS
									+ building.floors[NUM_FLOORS - 1
											- INIT_FLOOR_RIGHT].h
									- building.floors[NUM_FLOORS - 1
											- INIT_FLOOR_RIGHT].y);
	building.right_elevator.cabin.w = building.right_elevator.w;
	building.right_elevator.cabin.h = building.right_elevator.cabin.y
			+ building.floors[NUM_FLOORS - 1 - INIT_FLOOR_RIGHT].h
			- building.floors[NUM_FLOORS - 1 - INIT_FLOOR_RIGHT].y;

}

void drawBuilding() {

	rectfill(screen, SCREEN_W * 0.05, SCREEN_H * 0.12, SCREEN_W * 0.9,
			SCREEN_H * 0.9, BUILDING_COLOR);
	drawFloor();
	rectfill(screen, building.left_elevator.x, building.left_elevator.y,
			building.left_elevator.w, building.left_elevator.h, ELEVATOR_COLOR);
	rectfill(screen, building.right_elevator.x, building.right_elevator.y,
			building.right_elevator.w, building.right_elevator.h,
			ELEVATOR_COLOR);
	drawCabin(building.left_elevator.cabin.x, building.left_elevator.cabin.y,
			building.left_elevator.cabin.w, building.left_elevator.cabin.h);
	drawCabin(building.right_elevator.cabin.x, building.right_elevator.cabin.y,
			building.right_elevator.cabin.w, building.right_elevator.cabin.h);

}
void drawFloor() {
	int i;
	for (i = 0; i < NUM_FLOORS; i++)
		rectfill(screen, building.floors[i].x, building.floors[i].y,
				building.floors[i].w, building.floors[i].h, FLOOR_COLOR);

}
void drawCabinContent() {
	PERSON *person = NULL;
	CABIN *cabin = NULL;
	int i = 0, j = 0;
	char text[5];
	cabin = &building.left_elevator.cabin;
	for (j = 0; j < 2; j++) {
		for (i = 0; i < cabin->current_capacity; i++) {
			person = cabin->users[i];
			sprintf(text, "%d", person->dest);
			rectfill(screen, person->x, person->y, person->w, person->h,
					PERSON_IN_CABIN_COLOR);
			textout_ex(screen, font, text, person->x + 2, person->y + 3,
					TEXT_COLOR, -1);
			line(screen, person->w, person->y, person->w, person->h,
					LINE_COLOR);
		}
		cabin = &building.right_elevator.cabin;
	}
}
void drawFloorContent() {

	PERSON *person = NULL;
	FLOOR *b_floor = NULL;
	char text[5];
	int i = 0, j = 0;

	for (j = 0; j < NUM_FLOORS; j++) {
		b_floor = &building.floors[j];
		for (i = 0; i < b_floor->num_waiting; i++) {
			person = b_floor->people[i];
			if (person == NULL )
				return;

			sprintf(text, "%d", person->dest);
			rectfill(screen, person->x, person->y, person->w, person->h,
					PERSON_IN_FLOOR_COLOR);
			line(screen, person->w, person->y, person->w, person->h,
					LINE_COLOR);
			textout_ex(screen, font, text, person->x - 11, person->y + 3,
					TEXT_COLOR, -1);
		}
	}
}
void shiftPersonInElevator(PERSON *person, unsigned int elevator) {
	CABIN *cabin = NULL;

	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;

	person->y = cabin->y;
	person->h = cabin->h;
}
int isAtFloor(unsigned int elevator) {
	CABIN *cabin = NULL;

	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;

	PERSON *p;
	int i;
	int fl = cabin->current_floor;
	if (cabin->direction == DIRECTION_UP)
		fl++;
	else if (cabin->direction == DIRECTION_DOWN)
		fl--;
	else
		return 0;
	for (i = 0; i < cabin->current_capacity; i++) {
		p = cabin->users[i];
		if (p->dest == fl) {
			return 1;

		}
	}
	if((building.floors[NUM_FLOORS - 1 - fl].num_waiting && cabin->current_capacity!=MAX_CAPACITY))
		return 1;
	return 0;
}
void slideElevator(unsigned int elevator) {
	CABIN *cabin = NULL;
	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;
	cabin->status = ELEVATOR_MOVING;
	if (cabin->direction == DIRECTION_UP) {
		if (!isAtFloor(elevator)) {
			cabin->h -= ELEVATOR_ACC_MOVING;
			cabin->y -= ELEVATOR_ACC_MOVING;
		} else {
			cabin->h -= ELEVATOR_ACC_STOPPING;
			cabin->y -= ELEVATOR_ACC_STOPPING;
		}
	} else if (cabin->direction == DIRECTION_DOWN) {
		if (!isAtFloor(elevator)) {
			cabin->h += ELEVATOR_ACC_MOVING;
			cabin->y += ELEVATOR_ACC_MOVING;
		} else {
			cabin->h += ELEVATOR_ACC_STOPPING;
			cabin->y += ELEVATOR_ACC_STOPPING;
		}
	}

	int i;
	for (i = 0; i < cabin->current_capacity; i++) {
		shiftPersonInElevator(cabin->users[i], elevator);
	}

}
void updateCabinPosition(unsigned int elevator, unsigned int b_floor) {
	int current_pos;
	CABIN *cabin = NULL;
	//printf("cabin position updated\n");

	if (elevator == RIGHT_ELEVATOR) {
		cabin = &building.right_elevator.cabin;
		printf("differencve %f\n",
				fabs(cabin->h - building.floors[NUM_FLOORS - 1 - b_floor].h));
		slideElevator(elevator);

		if (fabs(
				cabin->h
						- building.floors[NUM_FLOORS - 1 - b_floor].h)>CABIN_THOLD)

			return;
		cabin->status = ELEVATOR_STOP;
		current_pos = building.right_elevator.cabin.current_floor;
		building.right_elevator.cabin.x = building.right_elevator.x;
		building.right_elevator.cabin.y = building.right_elevator.y
				+ (NUM_FLOORS - 1 - b_floor)
						* ((building.h / (NUM_FLOORS + 1)) / NUM_FLOORS
								+ building.floors[NUM_FLOORS - 1 - b_floor].h
								- building.floors[NUM_FLOORS - 1 - b_floor].y);
		building.right_elevator.cabin.w = building.right_elevator.w;
		building.right_elevator.cabin.h = building.right_elevator.cabin.y
				+ building.floors[NUM_FLOORS - 1 - b_floor].h
				- building.floors[NUM_FLOORS - 1 - b_floor].y;
		cabin->current_floor = b_floor;

	} else if (elevator == LEFT_ELEVATOR) {
		cabin = &building.left_elevator.cabin;
		slideElevator(elevator);

		if (fabs(
				cabin->h
						- building.floors[NUM_FLOORS - 1 - b_floor].h)>CABIN_THOLD)

			return;
		cabin->status = ELEVATOR_STOP;
		current_pos = building.right_elevator.cabin.current_floor;
		building.left_elevator.cabin.x = building.left_elevator.x;
		building.left_elevator.cabin.y = building.left_elevator.y
				+ (NUM_FLOORS - 1 - b_floor)
						* ((building.h / (NUM_FLOORS + 1)) / NUM_FLOORS
								+ building.floors[NUM_FLOORS - 1 - b_floor].h
								- building.floors[NUM_FLOORS - 1 - b_floor].y);
		building.left_elevator.cabin.w = building.left_elevator.w;
		building.left_elevator.cabin.h = building.left_elevator.cabin.y
				+ building.floors[NUM_FLOORS - 1 - b_floor].h
				- building.floors[NUM_FLOORS - 1 - b_floor].y;
		cabin->current_floor = b_floor;
	}
	int i = 0;
	for (i = 0; i < cabin->current_capacity; i++) {
		shiftPersonInElevator(cabin->users[i], elevator);
	}

}
void drawCabin(float x, float y, float w, float h) {
	rectfill(screen, x, y, w, h, CABIN_COLOR);
}

int addPersonToCabin(unsigned int elevator, unsigned int dest, REQUEST *r) {
	PERSON *person = (PERSON *) malloc(sizeof(PERSON));
	CABIN *cabin;
	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;
	else
		return -1;

	if (!cabin->current_capacity) {
		person->x = cabin->x;
		person->y = cabin->y;
		person->w = cabin->x + (cabin->w - cabin->x) / MAX_CAPACITY;
		person->h = cabin->h;
		person->dest = dest;
		person->mine = r;
		cabin->current_capacity++;
		cabin->users[cabin->current_capacity - 1] = person;
	} else if (cabin->current_capacity
			> 0&& cabin->current_capacity<MAX_CAPACITY) {
		int pos = cabin->current_capacity - 1;
		person->x = cabin->users[pos]->w + 1;
		person->y = cabin->y;
		person->w = cabin->users[pos]->w + (cabin->w - cabin->x) / MAX_CAPACITY;
		person->h = cabin->h;
		person->dest = dest;
		person->mine = r;
		cabin->current_capacity++;
		cabin->users[cabin->current_capacity - 1] = person;
	}

//line(screen,person->w,person->y,person->w,person->h,makecol(255,0,0));

	return 0;
}
void addPersonToFloor(unsigned int current_floor, unsigned int dest, REQUEST *r) {
	FLOOR *b_floor = NULL;
	if (current_floor >= NUM_FLOORS)
		return;
	printf("person added to floor\n");
	b_floor = &building.floors[NUM_FLOORS - 1 - current_floor];

	PERSON *person = (PERSON *) malloc(sizeof(PERSON));
	person->id = (NUM_FLOORS - 1 - current_floor) * 10 + b_floor->num_waiting;
	person->dest = dest;
	if (b_floor->num_waiting >= MAX_CAPACITY_FLOOR)
		return;
	if (!b_floor->num_waiting) {
		person->x = b_floor->x;
		person->y = b_floor->y;
		person->w = b_floor->x
				+ ((b_floor->w - b_floor->x) / MAX_CAPACITY_FLOOR);
		person->h = b_floor->h;
		person->mine = r;
		b_floor->people[0] = person;
		b_floor->num_waiting++;

	} else if (b_floor->num_waiting
			> 0&& b_floor->num_waiting < MAX_CAPACITY_FLOOR) {
		int pos;
		for (pos = 0; pos <= MAX_CAPACITY_FLOOR; pos++)
			if (b_floor->people[pos] == NULL ) {
				//printf("second person added\n");getchar();
				b_floor->people[pos] = person;
				break;
			}
		person->x = b_floor->people[pos]->w - 1;
		person->y = b_floor->y;
		person->w = b_floor->people[pos]->w
				+ (b_floor->w - b_floor->x) / MAX_CAPACITY_FLOOR;
		person->h = b_floor->h;
		person->mine = r;

		b_floor->num_waiting++;

	} else {
		printf("rare case found!!\n");
		getchar();
	}

}

void evelatorDisplay() {

	sprintf(disp_text, "Total requests created: %d. Pending requests: %d ",
			building.total_requests, building.pending_requests);
	textout_ex(screen, font, disp_text, 20, 20, 1, -1);
	sprintf(disp_text,
			"served requests LEFT ELEVATOR: %d.served requests RIGHT ELEVATOR: %d ",
			building.left_elevator.served_requests,
			building.right_elevator.served_requests);
	textout_ex(screen, font, disp_text, 20, 40, 1, -1);
	textout_centre_ex(screen, font, "Created by Psenjen Nelson Kigen", 150,
			SCREEN_H - 20, makecol(0, 0, 0), -1);

}
