/*
 * test.c
 *
 *  Created on: 5 Feb 2013
 *      Author: nkigen
 */
#include<time.h>
#include "elevator_logic.h"
#include <pthread.h>
#include <semaphore.h>

typedef struct {

} DATA;
DATA *dta;

sem_t mutex, req;
unsigned int NUM_FLOORS;
void init_semaphore() {
	sem_init(&mutex, 0, 1);
}
void *control_left(void *p) {
	struct timespec t;
	int period = PERIOD_CONTROL_LEFT;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);

	while (1) {
		printf("left worked num waiting %d\n",
				building.left_elevator.cabin.current_capacity);

		sem_wait(&mutex);
		moveCabin(LEFT_ELEVATOR);
		sem_post(&mutex);

		sem_wait(&mutex);
		serveRequest(LEFT_ELEVATOR);
		sem_post(&mutex);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
	}
}
void *control_right(void *p) {
	struct timespec t;
	int period = PERIOD_CONTROL_RIGHT;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);

	while (1) {
		printf("right worked num waiting %d\n",
				building.right_elevator.cabin.current_capacity);
		sem_wait(&mutex);
		moveCabin(RIGHT_ELEVATOR);
		sem_post(&mutex);

		sem_wait(&mutex);
		serveRequest(RIGHT_ELEVATOR);
		sem_post(&mutex);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);

	}

}
void *generate_traffic(void *p) {
	struct timespec t;
	int period = PERIOD_REQUEST;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);

	while (1) {

		printf("traffic worked: current queue: %d\n",
				building.pending_requests);
		sem_wait(&mutex);
		createRequest();
		sem_post(&mutex);
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
	}

}
int main(int argc, char **argv) {

	if (argc > 1) {
		if (sscanf(argv[1], "%d", &NUM_FLOORS) <= 0)
			NUM_FLOORS = DEFAULT_NUM_FLOORS;
		if(NUM_FLOORS>DEFAULT_NUM_FLOORS || NUM_FLOORS<MINIMUM_NUM_FLOORS)
			NUM_FLOORS = DEFAULT_NUM_FLOORS;
	} else if (argc == 1)
		NUM_FLOORS = DEFAULT_NUM_FLOORS;
	struct timespec t;
	int period = PERIOD_MAIN;
	clock_gettime(CLOCK_MONOTONIC, &t);
	time_add_ms(&t, period);
	init_semaphore();
	pthread_t ctrl_left, ctrl_right, ctrl_traf;
	pthread_attr_t attr_left, attr_right, attr_traf;
	int a, b, c;
	pthread_attr_init(&attr_left);
	pthread_attr_init(&attr_right);
	pthread_attr_init(&attr_traf);
	struct sched_param mypar;
	mypar.sched_priority = 20;

	allegro_init();
	install_keyboard();
	set_window_title("Elevator Simulator");
	set_gfx_mode(GFX_AUTODETECT_WINDOWED, X_LEN, Y_LEN, 0, 0);
	//request_refresh_rate(60);
	buildingInit();
	set_palette(desktop_palette);

//
//	pthread_attr_setinheritsched(&attr_left, PTHREAD_EXPLICIT_SCHED);
//	pthread_attr_setschedpolicy(&attr_left, SCHED_RR);
//
//	pthread_attr_setinheritsched(&attr_right, PTHREAD_EXPLICIT_SCHED);
//	pthread_attr_setschedpolicy(&attr_right, SCHED_RR);
//
//	pthread_attr_setinheritsched(&attr_traf, PTHREAD_EXPLICIT_SCHED);
//	pthread_attr_setschedpolicy(&attr_traf, SCHED_RR);
//
//	pthread_attr_setschedparam(&attr_left, &mypar);
//	pthread_attr_setschedparam(&attr_right, &mypar);
//	mypar.sched_priority = 60;
//	pthread_attr_setschedparam(&attr_traf, &mypar);

	a = pthread_create(&ctrl_left, &attr_left, control_left, dta);
	b = pthread_create(&ctrl_right, &attr_right, control_right, dta);
	c = pthread_create(&ctrl_traf, &attr_traf, generate_traffic, dta);
	printf("a %d b %d c %d\n", a, b, c);

	while (!key[KEY_ESC]) {
		acquire_screen();
		clear_to_color(screen, makecol(144, 255, 0));
		rectfill(screen, 0, 0, SCREEN_W, SCREEN_H * 0.1,
				makecol(255, 255, 255));
		drawBuilding();
		evelatorDisplay();
		sem_wait(&mutex);
		drawCabinContent();
		sem_post(&mutex);

		sem_wait(&mutex);
		drawFloorContent();
		sem_post(&mutex);
		release_screen();
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL );
		time_add_ms(&t, period);
		//	clear(screen);
		//printf("Daddy working pending requests %d\n",building.pending_requests);
	}
	pthread_join(ctrl_left, NULL );
	pthread_join(ctrl_right, NULL );
	pthread_join(ctrl_traf, NULL );
	pthread_attr_destroy(&attr_left);
	pthread_attr_destroy(&attr_right);
	pthread_attr_destroy(&attr_traf);
	allegro_exit();
	return 0;
}
END_OF_MAIN();
