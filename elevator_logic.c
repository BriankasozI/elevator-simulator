/*
 * elevator_logic.c
 *
 *  Created on: 7 Feb 2013
 *      Author: nkigen
 */
#include "elevator_logic.h"
extern unsigned int NUM_FLOORS;
void time_add_ms(struct timespec *t, int ms) {
	t->tv_sec += ms / 1000;
	t->tv_nsec += (ms % 1000) * 1000000;
	if (t->tv_nsec > 1000000000) {
		t->tv_nsec -= 1000000000;
		t->tv_sec += 1;
	}
}

int getNearestElevator(int current_floor, int target) {
	int my_dir;
	if (current_floor > target)
		my_dir = DIRECTION_DOWN;
	else if (current_floor < target)
		my_dir = DIRECTION_UP;
	else
		return -1;
	int e_1 = building.left_elevator.cabin.direction;
	int e_2 = building.right_elevator.cabin.direction;
	if (e_1 == my_dir)
		return LEFT_ELEVATOR;
	if (e_2 == my_dir)
		return RIGHT_ELEVATOR;

	return LEFT_ELEVATOR;

}
void createRequest(void) {
	unsigned int iseed = (unsigned int) time(NULL );
	srand(iseed);
	unsigned int th = (int) (100.0 * rand() / (RAND_MAX + 1.0));
	//printf("th %d\n", th);
	if (th > REQ_THRESHHOLD) {
		unsigned int frm = (int) ((double) NUM_FLOORS * rand()
				/ (RAND_MAX + 1.0));
		unsigned int to =
				(int) ((double) NUM_FLOORS * rand() / (RAND_MAX + 1.0));
		//printf("to %d from %d\n", to, frm);
		if (to == frm)
			return;
		if (building.pending_requests >= MAX_REQUESTS || frm > (NUM_FLOORS - 1)
				|| to > (NUM_FLOORS - 1))
			return;

		if ((building.floors[NUM_FLOORS - 1 - frm]).num_waiting
				>= MAX_CAPACITY_FLOOR)
			return;
		requestElevator(frm, to);

	}
}
void requestElevator(unsigned int from, unsigned int to) {
	//printf("elevator request from %d to %d", from, to);
	//REQUEST req;
//	req.from = from;
//	req.to = to;
//	req.status=REQUEST_PENDING;
	int i;
	REQUEST *r = NULL;
	for (i = 0; i < MAX_REQUESTS; i++) {
		if (building.request_queue[i].status == REQUEST_SERVED) {
			r = &building.request_queue[i];
			building.request_queue[i].from = from;
			building.request_queue[i].to = to;
			building.request_queue[i].status = REQUEST_PENDING;
			building.pending_requests++;
			building.total_requests++;
			addPersonToFloor(from, to, r);

			return;
		}
	}

}
int getDirection(unsigned int elevator) {
	/*implement scan algorithm*/
	unsigned int dir;
	CABIN *cabin = NULL;
	if (elevator == LEFT_ELEVATOR) {
		cabin = &building.left_elevator.cabin;
		//return DIRECTION_UP;
	} else if (elevator == RIGHT_ELEVATOR) {
		cabin = &building.right_elevator.cabin;
		//return DIRECTION_DOWN;
	}

	if (building.pending_requests == 0 && !cabin->current_capacity)
		return DIRECTION_NONE;
	int i = 0;

//	for (i = 0; i < building.pending_requests; i++) {
//		if ((cabin->current_floor > building.request_queue[i].from)
//				&& ((cabin->direction == DIRECTION_DOWN)
//						|| (cabin->direction == DIRECTION_NONE)))
//			return DIRECTION_DOWN;
//		if ((cabin->current_floor < building.request_queue[i].from)
//				&& ((cabin->direction == DIRECTION_UP)
//						|| (cabin->direction == DIRECTION_NONE)))
//			return DIRECTION_UP;
//
//	}

	if (building.floors[0].num_waiting) {
		FLOOR f = building.floors[0];
		if (cabin->current_floor > f.id
				&& ((cabin->direction == DIRECTION_DOWN)
						|| (cabin->direction == DIRECTION_NONE)))
			return DIRECTION_DOWN;
		if (cabin->current_floor < f.id
				&& ((cabin->direction == DIRECTION_UP)
						|| (cabin->direction == DIRECTION_NONE)))
			return DIRECTION_UP;
	}
	if (building.floors[NUM_FLOORS - 1].num_waiting) {
		FLOOR f = building.floors[NUM_FLOORS - 1];
		if (cabin->current_floor > f.id
				&& ((cabin->direction == DIRECTION_DOWN)
						|| (cabin->direction == DIRECTION_NONE)))
			return DIRECTION_DOWN;
		if (cabin->current_floor < f.id
				&& ((cabin->direction == DIRECTION_UP)
						|| (cabin->direction == DIRECTION_NONE)))
			return DIRECTION_UP;
	}
	for (i = 0; i < NUM_FLOORS - 1 && cabin->current_capacity != MAX_CAPACITY;
			i++) {
		FLOOR f = building.floors[i];
		if (f.id == cabin->current_floor)
			continue;
		if (f.num_waiting) {

			if (cabin->current_floor > f.id
					&& ((cabin->direction == DIRECTION_DOWN)
							|| (cabin->direction == DIRECTION_NONE)))
				return DIRECTION_DOWN;
			if (cabin->current_floor < f.id
					&& ((cabin->direction == DIRECTION_UP)
							|| (cabin->direction == DIRECTION_NONE)))
				return DIRECTION_UP;
		}
	}
	for (i = 0; i < cabin->current_capacity; i++) {
		unsigned int d = cabin->users[i]->dest;
		if (d > cabin->current_floor)
			return DIRECTION_UP;
		if (d < cabin->current_floor)
			return DIRECTION_DOWN;
	}

	if (cabin->current_floor == 0)
		return DIRECTION_UP;
	if (cabin->current_floor == NUM_FLOORS - 1)
		return DIRECTION_DOWN;
	return DIRECTION_NONE;

}

void moveCabin(unsigned int elevator) {
	CABIN *cabin = NULL;

	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;

	int dir = getDirection(elevator);
	if (dir == DIRECTION_DOWN && cabin->current_floor != 0) {
		cabin->direction = DIRECTION_DOWN;
		cabin->status = ELEVATOR_MOVING;
			updateCabinPosition(elevator, cabin->current_floor - 1);

		return;
	}
	if (dir == DIRECTION_UP && cabin->current_floor != NUM_FLOORS - 1) {

		cabin->direction = DIRECTION_UP;
		cabin->status = ELEVATOR_MOVING;
			updateCabinPosition(elevator, cabin->current_floor + 1);


		return;
	}

	if (dir == DIRECTION_NONE) {
		cabin->status = ELEVATOR_STOP;
		cabin->direction = DIRECTION_NONE;
	}
}
void pickPerson(REQUEST rq, unsigned int elevator, REQUEST *r) {
	FLOOR *f = NULL;
	CABIN *cabin = NULL;

	if (elevator == LEFT_ELEVATOR) {
		cabin = &building.left_elevator.cabin;
		building.left_elevator.served_requests++;
	} else if (elevator == RIGHT_ELEVATOR) {
		cabin = &building.right_elevator.cabin;
		building.right_elevator.served_requests++;
	}
	f = &building.floors[NUM_FLOORS - 1 - cabin->current_floor];
	addPersonToCabin(elevator, rq.to, r);
	int i = 0;
	if (f->num_waiting > 1) {
		//printf("dummy 1\n");
	}
	for (i = f->num_waiting - 1; i >= 0; i--)
		if (r == f->people[i]->mine)
			break;
	int pos = i;
//if(pos<0){
//	checkFloors(f->id);
//	pos = f->num_waiting - 1;
//}
	PERSON *p = f->people[pos];
	f->num_waiting--;
	building.pending_requests--;
	//printf("person picked from floor %d \n", f->id);
	if (p != NULL )
		free(p);
	f->people[pos] = NULL;
}
void serveRequest(unsigned int elevator) {
	//checkFloors();
	CABIN *cabin = NULL;
	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;
	else
		return;

	int i = 0;
	PERSON *p = NULL;
	/*check for alighting persons*/
	for (i = 0; i < cabin->current_capacity; i++) {
		p = cabin->users[i];
		if (p->dest == cabin->current_floor && cabin->status==ELEVATOR_STOP) {
			if (p != cabin->users[cabin->current_capacity - 1]) {
				p->dest = cabin->users[cabin->current_capacity - 1]->dest;
				p->id = cabin->users[cabin->current_capacity - 1]->id;
			}

			AlightPerson(elevator, cabin->users[cabin->current_capacity - 1]);

		}
	}

//	REQUEST rq;
//
//	for (i = 0;
//			i < MAX_REQUESTS && cabin->current_capacity < cabin->max_capacity;
//			i++) {
//		rq = building.request_queue[i];
//
//		if (rq.status == REQUEST_PENDING) {
//			if (cabin->current_floor == rq.from
//					&& building.floors[NUM_FLOORS - 1 - rq.from].num_waiting) {
//				cabin->status = ELEVATOR_STOP;
//				pickPerson(rq, elevator);
//				cabin->status = ELEVATOR_MOVING;
//				building.request_queue[i].status = REQUEST_SERVED;
//
//			}
//		}
//	}

	int j = 0;
	REQUEST *rq = NULL;
	FLOOR *f = &building.floors[NUM_FLOORS - 1 - cabin->current_floor];
	for (j = f->num_waiting - 1; j >= 0; j--) {
		if (f->people[j] != NULL ) {
			if (cabin->current_capacity < cabin->max_capacity
					&& building.floors[NUM_FLOORS - 1 - cabin->current_floor].num_waiting  && cabin->status==ELEVATOR_STOP) {
				rq = f->people[j]->mine;
				cabin->status = ELEVATOR_STOP;
				pickPerson(*rq, elevator, rq);
				cabin->status = ELEVATOR_MOVING;
				rq->status = REQUEST_SERVED;

			}
		}
	}

}

void AlightPerson(unsigned int elevator, PERSON *p) {
	CABIN *cabin = NULL;

	if (elevator == LEFT_ELEVATOR)
		cabin = &building.left_elevator.cabin;
	else if (elevator == RIGHT_ELEVATOR)
		cabin = &building.right_elevator.cabin;
	cabin->current_capacity--;
	free(p);
}

void checkFloors(int j) {
	int i, counter;

	FLOOR *f = &building.floors[NUM_FLOORS - 1 - j];
	for (i = 0; i < MAX_CAPACITY_FLOOR; i++) {
		if (f->people[i] != NULL )
			counter++;
	}
	if (f->num_waiting != counter)
		f->num_waiting = counter;
	counter = 0;

}

